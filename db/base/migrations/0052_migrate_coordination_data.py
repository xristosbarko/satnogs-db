# Generated by Django 4.0.4 on 2022-05-23 05:55

from django.db import migrations


def migrate_coordination_data(apps, schema_editor):
    TransmitterEntry = apps.get_model('base', 'TransmitterEntry')

    transmitters_with_itu_url = TransmitterEntry.objects.exclude(coordination_url="").filter(coordination__contains="ITU")
    for transmitter in transmitters_with_itu_url:
        transmitter.itu_coordination['urls'].append(transmitter.coordination_url)
        transmitter.save()
    
    transmitters_with_iaru_url = TransmitterEntry.objects.exclude(coordination_url="").filter(coordination__contains="IARU")
    for transmitter in transmitters_with_iaru_url:
        transmitter.iaru_coordination_url=transmitter.coordination_url
        transmitter.save()

    TransmitterEntry.objects.filter(coordination='IARU Requested').update(iaru_coordination='IARU Uncoordinated')
    TransmitterEntry.objects.filter(coordination='IARU Declined').update(iaru_coordination='IARU Declined')
    TransmitterEntry.objects.filter(coordination='IARU Coordinated').update(iaru_coordination='IARU Coordinated')


def reverse_migrate_coordination_data(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0051_add_itu_coordination_field'),
    ]

    operations = [
        migrations.RunPython(migrate_coordination_data, reverse_migrate_coordination_data),
    ]
